---
title: Instalación
weight: 15
disableToc: true
---

Para ejecutar la aplicación se realizan los siguientes pasos.

1. Clonar repositorio.
2. Instalar dependencias.
3. Empezar la aplicación.

#### 1. Clonar repositorio

Primero comprobar que tiene instalado git, ejecute:
```bash
    git --version    
```
{{% notice info %}}
Si no tiene instalado git, revise la sección de [requerimientos](/frontend/requerimientos/#5-git) para instalar **GIT**
{{% /notice %}}

Para clonar el repositorio ejecute:
```bash
    git clone https://git-desarrollo.cns.gob.bo/ariel.torrez/cns-pgr.git
```
![Minion](/Frontend/instalar/images/clonar.png)

#### 2. Instalar dependencias

Primero comprobar que tiene instalado npm, ejecute:
```bash
    npm -v
```
{{% notice info %}}
Si no tiene instalado npm, revise la sección de [requerimientos](/frontend/requerimientos/#2-npm-node-package-manager) para instalar **npm**
{{% /notice %}}

Para instalar las dependencias ingrese a la carpeta del proyecto y ejecute:

```bash
    npm install
```
![Minion](/Frontend/instalar/images/npminstall.png?classes=shadow&width=60pc)

#### 3. Empezar la aplicación

Para ejecutar la aplicación en la terminal ejecute:

```bash
    npm start
```
![Minion](/Frontend/instalar/images/npmstart.png?classes=shadow&width=60pc)

La aplicación te redireccionara al **inicio de sesión**.

![Minion](/Frontend/instalar/images/inicio.png?classes=shadow&width=60pc)

Una vez iniciada la sesión podras ver la aplicación corriendo.

![Minion](/Frontend/instalar/images/dash.png?classes=shadow&width=60pc)

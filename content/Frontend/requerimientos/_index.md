---
title: Requerimientos
weight: 10
disableToc: true
---

Para poder trabajar con **Angular** se necesita tener instalado en el ordenador:

1. Node.js.
2. npm (Node package manager).
3. Angular CLI.
4. Visual Studio Code u otro IDE de su preferencia.
5. Git.
6. Github Desktop `opcional`

#### 1. Node.js

Para descargar Node accedemos a [Node js](https://nodejs.org/es) , dónde nos aparecen dos opciones para descargar. En este caso elegiremos la opción de la izquierda, que es la opción estable y la más recomendada para la mayoría.

![Magic](/Frontend/requerimientos/images/nodejs.png?classes=shadow&width=60pc)

#### 2. npm (Node package manager)

Las aplicaciones Angular, Angular CLI y Angular dependen de los paquetes npm para muchas características y funciones. Para descargar e instalar paquetes npm, necesita un administrador de paquetes npm. En el proyecto se utilizara la interfaz de línea de comandos del cliente npm, que se instala con **Node.js** de manera predeterminada. Para comprobar que tiene instalado el cliente npm, ejecute:

```bash
    npm -v
```

![Magic](/Frontend/requerimientos/images/npm.png?width=35pc)

#### 3. Angular CLI

Para instalar la CLI angular, abra una ventana de terminal y ejecute el siguiente comando:

```bash
    ng --version
```
![Magic](/Frontend/requerimientos/images/CLI.png?classes=shadow&width=60pc)

#### 4. Visual Studio Code

La descarga de Visual Studio Code la realizamos desde la web de [Visual Studio Code](https://code.visualstudio.com) y pulsando en el botón de descargar.

![Magic](/Frontend/requerimientos/images/vscode.png?classes=shadow&width=60pc)

#### 5. Git

A continuación, debemos instalar Git, para lo que realizamos la descarga desde [git-scm.com/downloads](https://git-scm.com/downloads).

De la misma forma que hicimos antes, elegimos nuestro sistema operativo, descargamos el instalador y lo ejecutamos.

![Magic](/Frontend/requerimientos/images/git.png?classes=shadow&width=60pc)

#### 6. Github Desktop `opcional`

Es la interfaz gráfica de github que simplifica el trabajo con git, en el caso de que no se lleven bien con la terminal y los comandos git puedes descargarlo desde  [desktop.github.com](https://desktop.github.com/)

![Magic](/Frontend/requerimientos/images/gitdesktop.png?classes=shadow&width=60pc)